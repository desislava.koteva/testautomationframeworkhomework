# TestAutomationFrameworkHomework



## Steps execute tests:

1. Clone the repository locally
2. Import project and assure it is build successfuly
3. Open config.properties file located at src/test/resources/config.properties and edit it. All values that need to be changed are located in **section ### JIRA ###.** Enter your values for the following properties (use your own JIRA project and credentials):
   * **jira.project.url**=
   * **jira.users.desi.username=**
   * **jira.users.desi.password=**

**NOTE: Example for correct project URL is - https://dk-a38-team2.atlassian.net/browse/DX**

4. All JIRA related tests are located in src/test/java/test/cases/jira
5. It is recommended to execute tests in the following order:
   1. LoginJiraTest - validates basic login functionality works
   2. CreateStoryTest - creates new issue of type story
   3. CreateBugTest - creates new issue of type bug
   4. LinkStoryAndBugTest - link the last two created bug and story

