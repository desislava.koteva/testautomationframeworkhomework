package test.cases.jira;

import com.telerikacademy.testframework.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import pages.jira.CreateIssuePage;
import pages.jira.LoginPage;
import pages.jira.ProjectPage;

public class CreateStoryTest extends JiraBaseTest{
    @Test
    public void userCanCreateNewStory(){
        //Arrange
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("desi");
        ProjectPage projectPage = new ProjectPage(actions.getDriver());

        //Act
        CreateIssuePage createIssuePage = projectPage.clickButtonCreate();
        createIssuePage.selectIssueType("Story");
        createIssuePage.typeSummary(Utils.getConfigPropertyByKey("jira.newStory.summary"));
        createIssuePage.clickCreateButton();

        //Assert
        actions.waitForElementPresent("jira.createIssuePage.successPopUp");
        Assert.assertNotNull("Fail to create user story", actions.getDriver().findElement(By.xpath(Utils.getUIMappingByKey
                ("jira.createIssuePage.successPopUp"))));
    }
}
