package test.cases.jira;

import com.telerikacademy.testframework.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import pages.jira.LoginPage;

public class LoginJiraTest extends JiraBaseTest {

    private LoginPage loginPage = new LoginPage(actions.getDriver());

    @Test
    public void userShouldBeAbleToLoginWithValidCredentials(){
        //Arrange
        //Act
        loginPage.loginUser("desi");
        actions.waitForElementPresent("jira.projectPage.buttonUserProfile");
        //Assert
        Assert.assertNotNull("Button user profile not present", actions.getDriver().findElement(By.xpath(Utils.getUIMappingByKey
                ("jira.projectPage.buttonUserProfile"))));
    }

    @Test
    public void userShouldNotBeAbleToLoginWithCorrectUserAndWrongPassword(){
        //Arrange
        //Act
        loginPage.loginUser("wrong");
        //Assert
        Assert.assertTrue("Login error message not visible", actions.isElementPresent("jira.loginPage.errorMessage"));
    }
}
