package test.cases.jira;

import com.telerikacademy.testframework.Utils;
import org.junit.Test;
import org.openqa.selenium.Keys;
import pages.jira.AllIssuesPage;
import pages.jira.IssuePage;
import pages.jira.LoginPage;
import pages.jira.ProjectPage;

public class LinkStoryAndBugTest extends JiraBaseTest{

    @Test
    public void linkLastCreatedStoryToLastCreatedBug(){
        //Arrange
        new LoginPage(actions.getDriver()).loginUser("desi");
        //Act
        AllIssuesPage allIssuesPage = new ProjectPage(actions.getDriver()).openAllIssues();
        String latestBugKey = allIssuesPage.getLatestBugKey();
        IssuePage issuePage = allIssuesPage.openLastCreatedStory();
        issuePage.clickLinkIssueButton();
        issuePage.typeInsideSearchForIssuesInput(latestBugKey + Keys.RETURN);
        issuePage.clickLinkButton();

        //Assert
        actions.waitForElementPresent("jira.issuePage.linkContainer");
        actions.assertElementPresent("jira.issuePage.linkContainer");
    }
}
