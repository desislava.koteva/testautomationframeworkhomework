package test.cases.jira;

import com.telerikacademy.testframework.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import pages.jira.CreateIssuePage;
import pages.jira.LoginPage;
import pages.jira.ProjectPage;

public class CreateBugTest extends JiraBaseTest {

    @Test
    public void userCanCreateBug(){
        //Arrange
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("desi");
        ProjectPage projectPage = new ProjectPage(actions.getDriver());
        CreateIssuePage createIssuePage = projectPage.clickButtonCreate();
        createIssuePage.selectIssueType("Bug");
        createIssuePage.typeSummary(Utils.getConfigPropertyByKey("jira.newBug.summary"));
        createIssuePage.typeDescription(Utils.getConfigPropertyByKey("jira.newBug.description"));
        createIssuePage.selectIssuePriority("Highest");
        createIssuePage.typeEnvironment(Utils.getConfigPropertyByKey("jira.newBug.environment"));
        createIssuePage.clickAssignToMe();
        //Act
        createIssuePage.clickCreateButton();
        //Assert
        actions.waitForElementPresent("jira.createIssuePage.successPopUp");
        Assert.assertNotNull("Fail to create bug", actions.getDriver().findElement(By.xpath(Utils.getUIMappingByKey
                ("jira.createIssuePage.successPopUp"))));
    }
}
