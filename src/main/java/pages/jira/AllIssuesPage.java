package pages.jira;

import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AllIssuesPage extends BasePage {

    public AllIssuesPage(WebDriver driver) {
        super(driver, "");
    }

    public IssuePage openLastCreatedStory(){
        actions.waitForElementToBeClickable("jira.allIssuesPage.lastStory");
        actions.clickElement("jira.allIssuesPage.lastStory");
        return new IssuePage(actions.getDriver());
    }

    public String getLatestBugKey(){
        actions.waitForElementPresent("jira.allIssuesPage.lastBug");
        String latestBugKey = driver.findElement(By.xpath(Utils.getUIMappingByKey("jira.allIssuesPage.lastBug"))).getText();
        Utils.LOGGER.info("Latest bug key is: " + latestBugKey);
        return latestBugKey;
    }
}
