package pages.jira;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class IssuePage extends BasePage {

    public IssuePage(WebDriver driver) {
        super(driver, "");
    }

    public void clickLinkIssueButton(){
        actions.waitForElementToBeClickable("jira.issuePage.linkIssueButton");
        actions.clickElement("jira.issuePage.linkIssueButton");
    }

    public void typeInsideSearchForIssuesInput(String value){
        actions.waitForElementToBeClickable("jira.issuePage.searchForIssues");
        actions.typeValueInField(value, "jira.issuePage.searchForIssues");
    }

    public void clickLinkButton(){
        actions.waitForElementToBeClickable("jira.issuePage.linkButton");
        actions.clickElement("jira.issuePage.linkButton");
    }
}
