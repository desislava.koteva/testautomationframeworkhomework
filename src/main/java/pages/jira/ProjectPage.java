package pages.jira;

import com.telerikacademy.testframework.pages.BasePage;
import org.checkerframework.checker.units.qual.C;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProjectPage extends BasePage {

    public ProjectPage(WebDriver driver) {
        super(driver, "jira.project.url");
    }

    public CreateIssuePage clickButtonCreate(){
        actions.waitForElementVisible("jira.projectPage.buttonCreate");
        actions.waitForElementPresent("jira.projectPage.buttonCreate");
        actions.clickElement("jira.projectPage.buttonCreate");
        return new CreateIssuePage(actions.getDriver());
    }

    public AllIssuesPage openAllIssues(){
        actions.waitForElementPresent("jira.projectPage.buttonIssues");
        actions.clickElement("jira.projectPage.buttonIssues");
        actions.waitForElementPresent("jira.projectPage.buttonAllIssues");
        actions.clickElement("jira.projectPage.buttonAllIssues");
        return new AllIssuesPage(actions.getDriver());
    }
}
