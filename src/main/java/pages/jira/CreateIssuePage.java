package pages.jira;

import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CreateIssuePage extends BasePage {

    public CreateIssuePage(WebDriver driver) {
        super(driver, "jira.project.url");
    }

    public void selectIssueType(String issueType){
        actions.waitFor(2000);
        actions.clickElement("jira.createIssuePage.issueType");

        String issueTypeTemplate = Utils.getUIMappingByKey("jira.createIssuePage.issueTypeSelection");
        String issueTypeLocator = String.format(issueTypeTemplate, issueType);
//        actions.waitForElementVisibleUntilTimeout(issueTypeLocator, 5);
        actions.waitForElementPresent(issueTypeLocator);
        driver.findElement(By.xpath(issueTypeLocator)).click();
    }

    public void typeSummary(String summary){
        actions.waitForElementVisible("jira.createIssuePage.summaryInput");
        actions.typeValueInField(summary, "jira.createIssuePage.summaryInput");
    }

    public void typeDescription(String description){
        actions.waitForElementVisible("jira.createIssuePage.description");
        actions.typeValueInField(description, "jira.createIssuePage.description");
    }

    public void typeEnvironment(String environment){
        actions.waitForElementVisible("jira.createIssuePage.environment");
        actions.typeValueInField(environment, "jira.createIssuePage.environment");
    }

    public void clickCreateButton(){
        actions.waitFor(2000);
        actions.clickElement("jira.createIssuePage.createButton");
    }

    public void clickAssignToMe(){
        actions.clickElement("jira.createIssuePage.assignToMe");
    }

    public void selectIssuePriority(String issuePriority){
        actions.waitFor(2000);
        actions.clickElement("jira.createIssuePage.issuePriority");

        String issuePriorityTemplate = Utils.getUIMappingByKey("jira.createIssuePage.issuePrioritySelection");
        String issuePriorityLocator = String.format(issuePriorityTemplate, issuePriority);
//        actions.waitForElementVisibleUntilTimeout(issueTypeLocator, 5);
        actions.waitForElementPresent(issuePriorityLocator);
        driver.findElement(By.xpath(issuePriorityLocator)).click();
    }
}
