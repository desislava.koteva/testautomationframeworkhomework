package pages.jira;

import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.trello.BaseTrelloPage;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver) {
        super(driver, "jira.project.url");
    }

    public void loginUser(String userKey) {
        String username = Utils.getConfigPropertyByKey("jira.users." + userKey + ".username");
        String password = Utils.getConfigPropertyByKey("jira.users." + userKey + ".password");

        actions.typeValueInField(username, "jira.loginPage.username");
        actions.clickElement("jira.loginPage.continueButton");
        actions.waitForElementToBeClickable("jira.loginPage.password");
        actions.typeValueInField(password, "jira.loginPage.password");
        actions.clickElement("jira.loginPage.continueButton");
    }

//    public WebElement getErrorMessageElement(){
//        actions.
//        actions.getDriver().findElement(By.xpath(""));
//    }
}
